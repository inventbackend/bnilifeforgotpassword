<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${success eq 'true'}">
	<div class="col-10 mx-auto text-center">Reset Password Success.</div>
	<div class="col-10 mx-auto text-center">Please close this window.</div>
	
</c:if>
<c:if test="${success eq 'false'}">
	<div class="col-10 mx-auto text-center">Reset Password Failed!</div>
</c:if>
