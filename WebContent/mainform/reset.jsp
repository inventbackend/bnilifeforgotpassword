<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"
	import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<base href="./">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="BNI Life App CMS">
<meta name="author" content="BNI Life x MobileCom">
<title>BNI Life Forgot Password</title>
<!-- Icons-->
<link href="https://unpkg.com/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
<link href="https://unpkg.com/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
<link href="https://unpkg.com/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="https://unpkg.com/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
<!-- Main styles for this application-->
<link href="mainform/css/style.css" rel="stylesheet">
<link href="mainform/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
<link href="mainform/css/custom.css" rel="stylesheet">
</head>
<body class="app flex-row mt-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-5">
				<div class="card-group">
					<div class="card p-4" style="box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.2);">
						<div class="card-body justify-content-center">
							<img src="mainform/img/BNI-Life-Logo.png" alt="BNI Life Mobile" class="img-fluid mx-auto d-block mb-5"
								style="width: 10rem;"> <input type="hidden" id="query" name="query" value="<c:out value="${query}"/>" /> <input
								type="hidden" id="isvalid" name="isvalid" value="<c:out value="${valid}"/>" />
							<c:if test="${valid eq 'true'}">
								<div id="resetdialog">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"> <i class="icon-lock"></i>
											</span>
										</div>
										<input id="new_password" class="form-control" type="password" placeholder="Enter New Password">
									</div>
									<div class="input-group mb-4">
										<div class="input-group-prepend">
											<span class="input-group-text"> <i class="icon-lock"></i>
											</span>
										</div>
										<input id="new_password_confirm" class="form-control" type="password" placeholder="Confirm New Password">
									</div>
									<div class="row">
										<div class="col-6 mx-auto">
											<button class="btn btn-primary px-4" type="submit" onClick="resetOK()">Set Password</button>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${valid eq 'false'}">
								<div id="resetdialog" class="row">
									<div class="col-10 mx-auto text-center">Link is not valid or expired</div>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- CoreUI and necessary plugins-->
	<script src="https://unpkg.com/jquery/dist/jquery.min.js"></script>
	<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
	<script src="https://unpkg.com/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/pace-progress/pace.min.js"></script>
	<script src="https://unpkg.com/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
	<script src="https://unpkg.com/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script>
		function resetOK() {
			var new_password = jQuery('#new_password').val();
			var new_password_confirm = jQuery('#new_password_confirm').val();
			var myRegxp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
			if (new_password !== new_password_confirm) {
				swal.fire('Error', 'Password Is Not Same With Confirm Password', 'error');
			} else if (new_password.length < 8) {
				swal.fire('Error', 'Password Minimum Length Is 8', 'error');
			} else if (new_password.length > 15) {
				swal.fire('Error', 'Password Maximum Length Is 15', 'error');
			} else if (new_password_confirm.length < 8) {
				swal.fire('Error', 'Confirm Password Minimum Length Is 8', 'error');
			} else if (new_password_confirm.length > 15) {
				swal.fire('Error', 'Confirm Password Maximum Length Is 15', 'error');
			} else if (myRegxp.test(new_password) == false) {
				swal.fire('Error', 'Password must contains numbers and letters', 'error');
			} else if (myRegxp.test(new_password_confirm) == false) {
				swal.fire('Error', 'Confirm Password must contains numbers and letters', 'error');
			} else {
				var query = jQuery('#query').val();
				jQuery.ajax({
				url : '${pageContext.request.contextPath}/set-password',
				type : 'POST',
				data : {
				"query" : query,
				"new_password" : new_password,
				"new_password_confirm" : new_password_confirm
				},
				dataType : 'text',
				success : function(data, textStatus, jqXHR) {
					$('#resetdialog').html('');
					$('#resetdialog').html(data);
					if (!data.includes('Failed')) {
						swal.fire('Success', 'New Password Has Been Set', 'success')
					}
				},
				error : function(data, textStatus, jqXHR) {
					swal.fire('Error', 'Change Password Failed', 'error');
				}
				});

			}
		}
	</script>
</body>
</html>
