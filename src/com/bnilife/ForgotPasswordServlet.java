package com.bnilife;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import adapter.Base64Adapter;
import adapter.EncryptAdapter;

@WebServlet(urlPatterns = { "/reset" }, name = "reset")
public class ForgotPasswordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    final static Logger logger = Logger.getLogger(ForgotPasswordServlet.class);

    public ForgotPasswordServlet() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String queryString = request.getParameter("q");
	String decodedQueryString = Base64Adapter.DecryptBase64(queryString);
	String dataString = "";
	boolean isValid = false;
	try {
	    dataString = EncryptAdapter.decrypt(decodedQueryString, "bnilife");
	    String[] dataStringArray = dataString.split(";");
	    List<String> dataList = Arrays.stream(dataStringArray).collect(Collectors.toList());
	    if (dataList.size() == 4) {
		String dateQuery = dataList.get(3);
		Date dateNow = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date dateToken = df.parse(dateQuery.replace("T", " "));
		long toSecond = 1000;
		int dateDiff = (int) (dateNow.getTime() - dateToken.getTime());
		int diffSecond = (int) (dateDiff / toSecond);
		if (diffSecond > 600) {
		    isValid = false;
		} else {
		    isValid = true;
		}
	    }
	} catch (Exception ex) {
	    logger.info("ERROR. page : /reset, Exception : " + ex.toString());
	}

	if (isValid) {
	    request.setAttribute("valid", "true");
	    request.setAttribute("query", queryString);
	} else {
	    request.setAttribute("valid", "false");
	    request.setAttribute("query", queryString);
	}
	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/reset.jsp");
	dispacther.forward(request, response);
    }

}
