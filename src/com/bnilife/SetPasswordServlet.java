package com.bnilife;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import adapter.Base64Adapter;
import adapter.EncryptAdapter;
import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

@WebServlet(urlPatterns = { "/set-password" }, name = "set-password")
public class SetPasswordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    final static Logger logger = Logger.getLogger(SetPasswordServlet.class);

    public SetPasswordServlet() {
	super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String query = request.getParameter("query");
	String new_password = request.getParameter("new_password");
	String decodedQueryString = Base64Adapter.DecryptBase64(query);
	String dataString = "";
	try {
	    dataString = EncryptAdapter.decrypt(decodedQueryString, "bnilife");
	} catch (Exception ex) {

	}
	String[] dataStringArray = dataString.split(";");
	List<String> dataList = Arrays.stream(dataStringArray).collect(Collectors.toList());
	String username = dataList.get(0);
	String category = dataList.get(1);
	String uuid = dataList.get(2);

	if (category.equalsIgnoreCase("resetpassword")) {
	    boolean isExists = CheckUserFromUUID(username, uuid);
	    if (isExists) {
		boolean success = ResetUserPassword(username, new_password);
		if (success) {
		    request.setAttribute("success", "true");
		    logger.info("SUCCESS. ResetUserPassword, username : " + username + ", new_password : " + new_password);
		} else {
		    request.setAttribute("success", "false");
		}
	    } else {
		request.setAttribute("success", "false");
	    }
	} else {
	    request.setAttribute("success", "false");
	}

	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/set-success.jsp");
	dispacther.forward(request, response);
    }

    private static boolean CheckUserFromUUID(String username, String uuid) {
	boolean success = false;
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	CachedRowSet jrs = null;
	String sql = "";
	try {
	    sql = "SELECT username, uuid FROM ms_user_reset_password WHERE username = ? AND uuid = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", uuid));
	    jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckUserFromUUID");
	    while (jrs.next()) {
		success = true;
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : CheckUserFromUUID, username : " + username + ", uuid : " + uuid + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

    private static boolean ResetUserPassword(String username, String password) {
	List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
	String sql = "";
	boolean success = false;
	try {
	    sql = "UPDATE ms_user SET password = ? WHERE username = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", password));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));
	    success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ResetUserPassword");
	    if (success) {
		sql = "DELETE FROM ms_user_reset_password WHERE username = ? ";
		listParam = new ArrayList<mdlQueryExecute>();
		listParam.add(QueryExecuteAdapter.QueryParam("string", username));
		success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ResetUserPassword");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. Function : ResetUserPassword, username : " + username + ", password : " + password + ", Exception:" + ex.toString(), ex);
	}
	return success;
    }

}
