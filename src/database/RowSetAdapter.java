package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.apache.log4j.Logger;
import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;

import com.sun.rowset.JdbcRowSetImpl;
import com.sun.rowset.RowSetFactoryImpl;

/**
 * Documentation
 * 
 */
public class RowSetAdapter {
    final static Logger logger = Logger.getLogger(RowSetAdapter.class);
    private static Connection conn;

    public static JdbcRowSet getJDBCRowSet() throws Exception {

	// Context ctx = new InitialContext();
	// Context env = (Context) ctx.lookup("java:comp/env");
	// final String ldriverclass = (String) env.lookup("driverclass");
	// final String lurl = (String) env.lookup("url");
	// final String lusername = (String) env.lookup("username");
	// final String lpassword = (String) env.lookup("password");

	// Class.forName("com.mysql.jdbc.Driver");
	// String databaseUrl = "jdbc:mysql://localhost:3306/inventorywarehouse";
	// String username = "root";
	// String password = "";

	// RowSetFactory rsFactory = RowSetProvider.newFactory();
	// JdbcRowSet jRowSet = rsFactory.createJdbcRowSet();

	// jRowSet.setUrl(lurl);
	// jRowSet.setUsername(lusername);
	// jRowSet.setPassword(lpassword);
	// jRowSet.setReadOnly(false);
	//
	// return jRowSet;

	InitialContext context = new InitialContext();
	DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/bnilife");
	conn = dataSource.getConnection();
	JdbcRowSetImpl jdbcRs = new JdbcRowSetImpl(conn);

	return jdbcRs;
    }

    public static Connection getConnection() throws Exception {

	try {
	    // Context ctx = new InitialContext();
	    // Context env = (Context) ctx.lookup("java:comp/env");
	    // final String ldriverclass = (String) env.lookup("driverclass");
	    // final String lurl = (String) env.lookup("url");
	    // final String lusername = (String) env.lookup("username");
	    // final String lpassword = (String) env.lookup("password");

	    // String connectionURL = "jdbc:mysql://localhost:3306/inventorywarehouse" ;
	    // String userName = "root" ;
	    // String password = "";

	    // Class.forName("com.mysql.jdbc.Driver");

	    // conn = DriverManager.getConnection(connectionURL, userName, password);

	    InitialContext context = new InitialContext();
	    DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/bnilife");

	    conn = dataSource.getConnection();

	} catch (Exception e) {
	    logger.error("FAILED = method : getConnection, Exception : " + e.toString());
	    e.printStackTrace();
	}

	return conn;
    }

}
